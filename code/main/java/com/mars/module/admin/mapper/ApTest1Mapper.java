package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.ApTest1;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 测试1Mapper接口
 *
 * @author mars
 * @date 2024-03-01
 */
public interface ApTest1Mapper extends BasePlusMapper<ApTest1> {

}
