/**
 * 分页查询请假列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/bizLeave/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询请假详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/bizLeave/query/' + id,
        method: 'get'
    })
}

/**
 * 新增请假
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/bizLeave/add',
        method: 'post',
        data: data
    })
}

function submitApproveApi(data) {
    return requests({
        url: '/admin/bizLeave/submitApprove',
        method: 'post',
        data: data
    })
}

/**
 * 修改请假
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/bizLeave/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除请假
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/bizLeave/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出请假
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/bizLeave/export',
        method: 'post',
        data: data
    })
}
