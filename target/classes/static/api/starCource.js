/**
 * 分页查询教程列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/starCource/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询教程详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/starCource/query/' + id,
        method: 'get'
    })
}

/**
 * 新增教程
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/starCource/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改教程
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/starCource/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除教程
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/starCource/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出教程
 *
 * @param query
 * @returns {*}
 */
function exportCource(query) {
    return requests({
        url: '/admin/starCource/export',
        method: 'get',
        params: query
    })
}
