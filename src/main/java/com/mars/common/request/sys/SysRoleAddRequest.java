package com.mars.common.request.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

import java.util.List;

/**
 * 角色新增DTO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRoleAddRequest {

    @NotEmpty
    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "备注")
    private String remark;

    @ApiModelProperty(value = "菜单")
    private List<Long> menuId;


}
