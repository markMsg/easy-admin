package com.mars.module.tool.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.GenControllerMethods;
import com.mars.module.tool.entity.GenTableColumn;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 控制器方法 数据层
 *
 * @author mars
 */
public interface GenControllerMethodsMapper extends BasePlusMapper<GenControllerMethods> {


}
