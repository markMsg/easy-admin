package com.mars.module.oss.controller;

import com.mars.common.result.R;
import com.mars.module.oss.service.IUploadService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 程序员Mars
 */
@Api(tags = "图片上传接口")
@RestController
@RequestMapping("/common/upload")
@AllArgsConstructor
public class FileController {

    private IUploadService fileService;

    /**
     * 上传接口
     *
     * @param file file
     * @return R
     */
    @ApiOperation(value = "图片上传")
    @CrossOrigin
    @PostMapping(value = "/file")
    public R<String> upload(@RequestParam(value = "file") MultipartFile file) throws Exception {
        return R.success(fileService.upload(file));
    }
}
