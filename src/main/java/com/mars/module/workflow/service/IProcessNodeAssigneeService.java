package com.mars.module.workflow.service;

import com.mars.module.workflow.entity.ProcessNodeAssignee;
import com.mars.common.response.PageInfo;
import com.mars.module.workflow.request.ProcessNodeAssigneeRequest;

import java.util.List;

/**
 * 审批人员接口
 *
 * @author mars
 * @date 2024-01-25
 */
public interface IProcessNodeAssigneeService {
    /**
     * 新增
     *
     * @param param param
     * @return ProcessNodeAssignee
     */
    ProcessNodeAssignee add(ProcessNodeAssigneeRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(ProcessNodeAssigneeRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return ProcessNodeAssignee
     */
    ProcessNodeAssignee getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<ProcessNodeAssignee>
     */
    PageInfo<ProcessNodeAssignee> pageList(ProcessNodeAssigneeRequest param);


    /**
     * 查询所有数据
     *
     * @return List<ProcessNodeAssignee>
     */
    List<ProcessNodeAssignee> list(ProcessNodeAssigneeRequest param);

    /**
     * 根据流程key获取审批人员
     *
     * @param flowKey flowKey
     * @return List<ProcessNodeAssignee>
     */
    List<ProcessNodeAssignee> getNodeAssigneeList(String flowKey);
}
