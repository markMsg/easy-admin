package com.mars.module.workflow.service;

import com.mars.common.response.PageInfo;
import com.mars.module.workflow.request.ListModelRequest;
import com.mars.module.workflow.request.ModelRequest;
import org.activiti.engine.repository.Model;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-22 17:14:10
 */
public interface IProcessModelService {


    /**
     * 创建模型
     *
     * @param request request
     * @return String
     */
    String create(ModelRequest request);

    /**
     * 部署模型
     *
     * @param request request
     */
    void deploy(ModelRequest request) throws IOException;

    /**
     * 导出模型
     *
     * @param request  request
     * @param response response
     */
    void export(ModelRequest request, HttpServletResponse response) throws IOException;


    /**
     * 删除模型
     *
     * @param ids ids
     */
    void remove(String ids);

    /**
     * 获取模型列表
     *
     * @param request request
     * @return PageInfo<Model>
     */
    PageInfo<Model> getModelList(ListModelRequest request);

}
