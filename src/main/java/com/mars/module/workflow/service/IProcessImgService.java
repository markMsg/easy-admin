package com.mars.module.workflow.service;

import java.io.IOException;
import java.io.InputStream;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-30 17:36:05
 */
public interface IProcessImgService {


    /**
     * 根据流程实例ID获取路程图片
     *
     * @param processInstanceId processInstanceId
     * @return String
     */
    String readProcessImg(String processInstanceId) throws IOException;
}
