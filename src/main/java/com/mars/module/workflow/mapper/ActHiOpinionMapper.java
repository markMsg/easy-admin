package com.mars.module.workflow.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.module.workflow.entity.ActHiOpinion;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Hp
 */
@Mapper
public interface ActHiOpinionMapper extends BaseMapper<ActHiOpinion> {


}
