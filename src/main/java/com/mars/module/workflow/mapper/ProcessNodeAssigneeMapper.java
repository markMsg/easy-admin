package com.mars.module.workflow.mapper;

import com.mars.module.workflow.entity.ProcessNodeAssignee;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 审批人员Mapper接口
 *
 * @author mars
 * @date 2024-01-25
 */
public interface ProcessNodeAssigneeMapper extends BasePlusMapper<ProcessNodeAssignee> {

}
