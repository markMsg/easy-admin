package com.mars.module.workflow.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Hp
 */
@Data
@ApiModel(description = "流程发起请求")
@Builder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class StartRequest implements Serializable {
    private static final long serialVersionUID = -6624177017837095888L;

    @NotBlank(message = "流程实例名称不能为空")
    @ApiModelProperty(value = "流程实例名称", required = true)
    private String flowName;

    @NotBlank(message = "流程定义key不能为空")
    @ApiModelProperty(value = "流程定义key, processDefinitionKey", required = true)
    private String flowKey;

    /**
     * 业务表单key不能为空
     */
    @NotBlank(message = "业务表单key不能为空")
    @ApiModelProperty(value = "业务表单key")
    private String businessKey;

    @ApiModelProperty(value = "流程实例发起人(申请人)", required = true)
    private String applyUser;

    /**
     * 租户id
     */
    @ApiModelProperty(value = "租户id")
    private String tenantId;

    /**
     * 流程变量
     */
    @ApiModelProperty(value = "流程变量")
    private Map<String, Object> variables = new HashMap<>();
}
