package com.mars.module.workflow.response;

import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-25 18:37:53
 */
@Data
public class ToDoTaskResponse {

    /**
     * 代办任务名称
     */
    private String taskName;
    /**
     * 审批人ID
     */
    private String assignee;
    /**
     * 流程实例ID
     */
    private String proInstanceId;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 业务key
     */
    private String businessKey;

}
