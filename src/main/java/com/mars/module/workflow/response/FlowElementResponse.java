package com.mars.module.workflow.response;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-02-21 09:46:05
 */
@Data
@ApiModel(value = "流程节点响应信息")
public class FlowElementResponse {

    private String id;

    private String name;

}
