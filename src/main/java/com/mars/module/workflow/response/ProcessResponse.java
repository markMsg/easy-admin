package com.mars.module.workflow.response;

import lombok.Data;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-25 16:17:49
 */
@Data
public class ProcessResponse {

    /**
     * 业务id
     */
    private String businessKey;

    /**
     * 流程实例id
     */
    private String procInstId;

    /**
     * 发起人id
     */
    private String startUserId;
}
