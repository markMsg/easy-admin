package com.mars.module.workflow.controller;

import com.mars.common.result.R;
import com.mars.module.workflow.request.ProcessDefinitionRequest;
import com.mars.module.workflow.request.ProcessImgRequest;
import com.mars.module.workflow.response.FlowElementResponse;
import com.mars.module.workflow.service.IProcessImgService;
import com.mars.module.workflow.service.IProcessNodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.FlowElement;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * 流程节点控制器
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-30 17:50:18
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "流程节点管理", tags = "流程节点管理")
@RequestMapping("/workflow")
public class ProcessNodeController {


    private final IProcessNodeService processNodeService;

    /**
     * 获取所有节点列表
     *
     * @param request request
     * @return R
     */
    @ApiOperation(value = "获取所有节点")
    @PostMapping("/node/getNodeList")
    public R<List<FlowElementResponse>> getNodeList(@RequestBody ProcessDefinitionRequest request) {
        return R.success(processNodeService.getNodeList(request.getDeploymentId(), request.getKey()));
    }

}
