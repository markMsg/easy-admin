package com.mars.module.workflow.controller;

import com.mars.common.result.R;
import com.mars.module.workflow.request.ProcessDefinitionRequest;
import com.mars.module.workflow.request.ProcessImgRequest;
import com.mars.module.workflow.service.IProcessImgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * 功能描述
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2024-01-30 17:50:18
 */
@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "流程引擎图片管理", tags = "流程引擎图片管理")
@RequestMapping("/workflow")
public class ProcessImgController {


    private final IProcessImgService processImgService;

    /**
     * 根据流程实例ID获取图片
     *
     * @param request request
     * @return R
     */
    @ApiOperation(value = "根据流程实例ID获取图片")
    @PostMapping("/image/readProcessImg")
    public R<String> readProcessImg(@RequestBody ProcessImgRequest request) throws IOException {
        return R.success(processImgService.readProcessImg(request.getProInstanceId()));
    }

}
