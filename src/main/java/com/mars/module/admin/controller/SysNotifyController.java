package com.mars.module.admin.controller;


import java.util.Arrays;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.SysNotify;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysNotifyService;
import com.mars.module.admin.request.SysNotifyRequest;

/**
 * 通知公告控制层
 *
 * @author mars
 * @date 2023-12-06
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "通知公告接口管理", tags = "通知公告接口管理")
@RequestMapping("/admin/sysNotify")
public class SysNotifyController {

    private final ISysNotifyService iSysNotifyService;

    /**
     * 分页查询通知公告列表
     */
    @ApiOperation(value = "分页查询通知公告列表")
    @PostMapping("/pageList")
    public R<PageInfo<SysNotify>> pageList(@RequestBody SysNotifyRequest sysNotify) {
        return R.success(iSysNotifyService.pageList(sysNotify));
    }

    /**
     * 获取通知公告详细信息
     */
    @ApiOperation(value = "获取通知公告详细信息")
    @GetMapping(value = "/query/{id}")
    public R<SysNotify> detail(@PathVariable("id") Long id) {
        return R.success(iSysNotifyService.getById(id));
    }

    /**
     * 新增通知公告
     */
    @Log(title = "新增通知公告", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增通知公告")
    @PostMapping("/add")
    public R<Void> add(@RequestBody SysNotifyRequest sysNotify) {
        iSysNotifyService.add(sysNotify);
        return R.success();
    }

    /**
     * 修改通知公告
     */
    @Log(title = "修改通知公告", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改通知公告")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody SysNotifyRequest sysNotify) {
        iSysNotifyService.update(sysNotify);
        return R.success();
    }

    /**
     * 删除通知公告
     */
    @Log(title = "删除通知公告", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除通知公告")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iSysNotifyService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }


    /**
     * 推送通知
     */
    @Log(title = "推送通知", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "推送通知")
    @PostMapping("/push")
    public R<Void> push(@RequestBody SysNotifyRequest sysNotify) {
        iSysNotifyService.sendNotify(sysNotify);
        return R.success();
    }
}
