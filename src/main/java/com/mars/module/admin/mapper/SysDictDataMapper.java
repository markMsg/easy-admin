package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysDictData;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 字典数据Mapper接口
 *
 * @author mars
 * @date 2023-11-18
 */
public interface SysDictDataMapper extends BasePlusMapper<SysDictData> {

}
