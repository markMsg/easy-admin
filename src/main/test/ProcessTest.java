import com.mars.EasyAdminApplication;
import com.mars.common.base.UserContextInfo;
import com.mars.common.request.tool.GenCodeDeployRequest;
import com.mars.common.request.tool.GenCodeRemoveRequest;
import com.mars.framework.context.ContextUserInfoThreadHolder;
import com.mars.module.tool.request.GenTableImportRequest;
import com.mars.module.tool.service.ITableGenerationService;
import com.mars.module.workflow.service.IProcessDefinitionService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EasyAdminApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProcessTest {

    @Autowired
    private IProcessDefinitionService processDefinitionService;

    /**
     * 获取流程图
     */
    @Test
    public void getProcessImageTest() throws IOException {
        processDefinitionService.getProcessImageBase64("125004");
    }


}
