/**
 * 分页查询模型列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/workflow/modeler/list',
        method: 'post',
        data: data
    })
}

/**
 * 创建模型
 * @param data data
 * @returns {*}
 */
function createModel(data) {
    return requests({
        url: '/workflow/modeler/create',
        method: 'post',
        data: data
    })
}

